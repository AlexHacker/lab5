import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class GUI {
	private JFrame f;
	private JLabel l;
	private ImmutableClass ic;
	private MutableClass mc;

	public GUI(JLabel lab) {
		f = new JFrame();
		l = lab;
		f.addWindowListener(new WindowListener() {

			public void windowActivated(WindowEvent event) {

			}

			public void windowClosed(WindowEvent event) {

			}

			public void windowClosing(WindowEvent event) {
				Object[] options = { "��", "���!" };
				int n = JOptionPane.showOptionDialog(event.getWindow(),
						"������� ����?", "�������������",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
				if (n == 0) {
					event.getWindow().setVisible(false);
					System.exit(0);
				}
			}

			public void windowDeactivated(WindowEvent event) {

			}

			public void windowDeiconified(WindowEvent event) {

			}

			public void windowIconified(WindowEvent event) {

			}

			public void windowOpened(WindowEvent event) {

			}

		});
		f.setJMenuBar(createMenu());
		f.setBounds(400, 200, 200, 200);
		f.setMinimumSize(new Dimension(950, 700));
		f.setTitle("Immutable");
		f.setResizable(false);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
		f.setContentPane(createStartPanel());

	}

	private JMenuBar createMenu() {
		JMenuBar menu = new JMenuBar();
		JMenu choice = new JMenu("�����");
		JMenuItem item = new JMenuItem("�������");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				f.getContentPane().removeAll();
				f.setContentPane(createStartPanel());
				f.getContentPane().revalidate();
			}
		});
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		choice.add(item);
		item = new JMenuItem("������������");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				f.getContentPane().removeAll();
				f.setContentPane(immutablePanel());
				f.getContentPane().revalidate();
			}
		});
		choice.add(item);
		choice.setMnemonic(KeyEvent.VK_ALT);
		item = new JMenuItem("����������");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				f.getContentPane().removeAll();
				f.setContentPane(mutablePanel());
				f.getContentPane().revalidate();
			}
		});
		choice.add(item);
		menu.add(choice);
		return menu;
	}

	private JPanel createStartPanel() {
		// TODO Auto-generated method stub
		JPanel start = new JPanel();
		start.setLayout(new BorderLayout());
		l.setFont(new Font("Veranda", Font.BOLD, 36));
		l.setHorizontalAlignment(SwingConstants.CENTER);
		l.setVerticalAlignment(SwingConstants.CENTER);
		TitledBorder border = new TitledBorder("�������");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitleFont(new Font("Veranda", Font.BOLD, 20));
		l.setBorder(border);
		start.add(l, BorderLayout.CENTER);
		return start;
	}

	private JPanel immutablePanel() {
		JPanel immutable = new JPanel();
		TitledBorder border = new TitledBorder("Immutable");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitleFont(new Font("Veranda", Font.BOLD, 20));
		immutable.setBorder(border);
		immutable.setLayout(new BorderLayout());
		JLabel l = new JLabel("������ � ������������� ��������");
		l.setForeground(Color.BLUE);
		immutable.add(l, BorderLayout.NORTH);
		JLabel txt = new JLabel();
		txt.setBackground(Color.WHITE);
		txt.setOpaque(true);
		txt.setVerticalAlignment(SwingConstants.TOP);
		txt.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JScrollPane scroll = new JScrollPane(txt);
		immutable.add(scroll, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();
		JButton create = new JButton("�������");
		JButton addInt = new JButton("��������� �����");
		JButton changeInt = new JButton("�������� �����");
		JButton changeString = new JButton("�������� ������");
		JButton addValue = new JButton("�������� ��������");
		JButton change = new JButton("��������");
		create.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = new ImmutableClass(5, new ArrayList<Integer>(), "first");
				txt.setText(ic.toString());
				txt.requestFocus();
				change.setEnabled(true);
				addInt.setEnabled(true);
				changeInt.setEnabled(true);
				changeString.setEnabled(true);
				addValue.setEnabled(true);
			}
		});
		change.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = ic.tryChange(ic.getInt(), ic.getList(), "change");
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += ic.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		addInt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = ic.addInt(1);
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += ic.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		changeInt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = ic.addInt(new Random().nextInt(100));
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += ic.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		changeString.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = ic.changeString();
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += ic.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		addValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ic = ic.addValue(2);
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += ic.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		change.setEnabled(false);
		addInt.setEnabled(false);
		changeInt.setEnabled(false);
		changeString.setEnabled(false);
		addValue.setEnabled(false);
		buttonPanel.add(create);
		buttonPanel.add(change);
		buttonPanel.add(addInt);
		buttonPanel.add(addValue);
		buttonPanel.add(changeInt);
		buttonPanel.add(changeString);
		immutable.add(buttonPanel, BorderLayout.SOUTH);
		return immutable;
	}

	private JPanel mutablePanel() {
		JPanel immutable = new JPanel();
		TitledBorder border = new TitledBorder("Mutable");
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitleFont(new Font("Veranda", Font.BOLD, 20));
		immutable.setBorder(border);
		immutable.setLayout(new BorderLayout());
		JLabel l = new JLabel("������ � ����������� ��������");
		l.setForeground(Color.BLUE);
		immutable.add(l, BorderLayout.NORTH);
		JLabel txt = new JLabel();
		txt.setBackground(Color.WHITE);
		txt.setOpaque(true);
		txt.setVerticalAlignment(SwingConstants.TOP);
		txt.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JScrollPane scroll = new JScrollPane(txt);
		immutable.add(scroll, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();
		JButton create = new JButton("�������");
		JButton addInt = new JButton("��������� �����");
		JButton changeInt = new JButton("�������� �����");
		JButton changeString = new JButton("�������� ������");
		JButton addValue = new JButton("�������� ��������");
		JButton change = new JButton("��������");
		JButton ser = new JButton("�������������");
		JButton deser = new JButton("���������������");
		create.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc = new MutableClass(5, new ArrayList<Integer>(), "first");
				txt.setText(mc.toString());
				txt.requestFocus();
				change.setEnabled(true);
				addInt.setEnabled(true);
				changeInt.setEnabled(true);
				changeString.setEnabled(true);
				addValue.setEnabled(true);
				ser.setEnabled(true);
				deser.setEnabled(true);
			}
		});
		ser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					FileOutputStream fos = new FileOutputStream("temp.ser");
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(mc);
					oos.flush();
					oos.close();
				} catch (Exception exc) {
					System.out.println(exc.toString());
				}
			}
		});
		deser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					FileInputStream fis = new FileInputStream("temp.ser");
					ObjectInputStream oin = new ObjectInputStream(fis);
					MutableClass mc2 = (MutableClass) oin.readObject();
					String s = txt.getText();
					s += "<html><br>----------------------------��������������-----------------------------<br>";
					s += mc2.toString();
					s += "<html><br>----------------------------��������������-----------------------------<br>";
					txt.setText(s);
					txt.requestFocus();
					oin.close();
				} catch (Exception exc) {
					System.out.println(exc.toString());
				} finally {
					new File("D:\\GitProjects\\Immutable\\temp.ser").delete();
				}
			}
		});
		change.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc.tryChange(mc.getInt(), mc.getList(), "change");
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += mc.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		addInt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc.addInt(1);
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += mc.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		changeInt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc.addInt(new Random().nextInt(100));
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += mc.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		changeString.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc.changeString();
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += mc.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		addValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mc.addValue();
				String s = txt.getText();
				s += "<html><br>---------------------------------------------------------<br>";
				s += mc.toString();
				txt.setText(s);
				txt.requestFocus();
			}
		});
		change.setEnabled(false);
		addInt.setEnabled(false);
		changeInt.setEnabled(false);
		changeString.setEnabled(false);
		addValue.setEnabled(false);
		ser.setEnabled(false);
		deser.setEnabled(false);
		buttonPanel.add(create);
		buttonPanel.add(change);
		buttonPanel.add(addInt);
		buttonPanel.add(addValue);
		buttonPanel.add(changeInt);
		buttonPanel.add(changeString);
		buttonPanel.add(ser);
		buttonPanel.add(deser);
		immutable.add(buttonPanel, BorderLayout.SOUTH);
		return immutable;
	}

}
