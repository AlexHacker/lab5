import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class MutableClass implements Serializable, ObjectInputValidation {
	private static final long serialVersionUID = 201505134;
	private int immutableField1;
	private ArrayList<Integer> immutableField2;
	private String immutableField3;
	private Date immutableField4;

	public MutableClass(int fld1, ArrayList<Integer> fld2, String fld3) {
		this.immutableField1 = fld1;
		this.immutableField2 = fld2;
		this.immutableField3 = fld3;
		this.immutableField4 = new Date();
	}

	public void tryChange(int fld1, ArrayList<Integer> fld2, String fld3) {
		this.immutableField1 = fld1;
		this.immutableField2 = fld2;
		this.immutableField3 = fld3;
	}

	public String toString() {
		return "<html>value: <font color = red>" + this.immutableField1
				+ "</font> <br>list: <font color = red>" + this.immutableField2
				+ "</font> <br>string: <font color = red>"
				+ this.immutableField3 + " </font><br>date: <font color = red>"
				+ this.immutableField4 + "</font>";
	}

	public void addValue() {
		if (this.immutableField2.size() == 0)
			this.immutableField2.add(1);
		else
			this.immutableField2.add(this.immutableField2
					.get(this.immutableField2.size() - 1) + 1);
	}

	public void changeInt(int value) {
		if (value == this.immutableField1)
			value++;
		this.immutableField1 = value;
	}

	public void addInt(int value) {
		this.immutableField1 = this.immutableField1 + value;
	}

	public void changeString() {
		StringBuffer buf = new StringBuffer(this.immutableField3);
		buf.reverse();
		this.immutableField3 = new String(buf);
	}

	public int getInt() {
		return this.immutableField1;
	}

	public ArrayList<Integer> getList() {
		return this.immutableField2;
	}

	public String getString() {
		return this.immutableField3;
	}

	/*
	 * private void writeObject(ObjectOutputStream stream) throws
	 * java.io.IOException { this.immutableField1 = this.immutableField1 >> 2;
	 * stream.defaultWriteObject(); }
	 * 
	 * private void readObject(ObjectInputStream stream) throws
	 * java.io.IOException, ClassNotFoundException { stream.defaultReadObject();
	 * this.immutableField1 = this.immutableField1 << 2; }
	 */

	@Override
	public void validateObject() throws InvalidObjectException {
		throw new InvalidObjectException("");

	}

}
