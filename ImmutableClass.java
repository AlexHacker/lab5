import java.util.ArrayList;
import java.util.Date;

public final class ImmutableClass {
	private final int immutableField1;
	private final ArrayList<Integer> immutableField2;
	private final String immutableField3;
	private final Date immutableField4;

	public ImmutableClass(int fld1, ArrayList<Integer> fld2, String fld3) {
		this.immutableField1 = fld1;
		this.immutableField2 = fld2;
		this.immutableField3 = fld3;
		this.immutableField4 = new Date();
	}

	public ImmutableClass tryChange(int fld1, ArrayList<Integer> fld2,
			String fld3) {
		return new ImmutableClass(fld1, fld2, fld3);
	}

	public String toString() {
		return "<html>value: <font color = red>" + this.immutableField1
				+ "</font> <br>list: <font color = red>" + this.immutableField2
				+ "</font> <br>string: <font color = red>"
				+ this.immutableField3 + " </font><br>date: <font color = red>"
				+ this.immutableField4 + "</font>";
	}

	public ImmutableClass addValue(int value) {
		@SuppressWarnings("unchecked")
		ArrayList<Integer> list = (ArrayList<Integer>) this.immutableField2
				.clone();
		if (list.size() == 0)
			list.add(1);
		else
			list.add(list.get(list.size() - 1) + 1);
		return new ImmutableClass(this.immutableField1, list,
				this.immutableField3);
	}

	public ImmutableClass changeInt(int value) {
		if (value == this.immutableField1)
			value++;
		return new ImmutableClass(value, this.immutableField2,
				this.immutableField3);
	}

	public ImmutableClass addInt(int value) {
		return new ImmutableClass(value + this.immutableField1,
				this.immutableField2, this.immutableField3);
	}

	public ImmutableClass changeString() {
		StringBuffer buf = new StringBuffer(this.immutableField3);
		buf.reverse();
		return new ImmutableClass(this.immutableField1, this.immutableField2,
				new String(buf));
	}

	public int getInt() {
		return this.immutableField1;
	}

	public ArrayList<Integer> getList() {
		return this.immutableField2;
	}

	public String getString() {
		return this.immutableField3;
	}

}
