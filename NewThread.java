import java.awt.Color;

import javax.swing.JLabel;

public class NewThread implements Runnable {
	private Thread t;
	private JLabel l;
	private String msg;
	private CallMe callMe;

	public NewThread(String m, CallMe c, JLabel lab) {
		l = lab;
		l.setForeground(Color.RED);
		msg = m;
		callMe = c;
		t = new Thread(this);
		t.start();
	}

	public void run() {
		synchronized (callMe) {
			callMe.call(l, msg);
		}

	}

	public Thread getThread() {
		return t;
	}

}
